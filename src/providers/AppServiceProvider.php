<?php

namespace aqsat_integration_bnpl\spoti\providers;

use aqsat_integration_bnpl\spoti\api\Main;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider{

    public function register() {

        if($this->app->runningInConsole()){

            $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

            $this->commands([  ]);
        }


        $this->registerConfig();

    }


    public function boot() {

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'spoti');

    }

    protected function registerConfig(){

        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('Spoti.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'spoti'
        );
        $this->app->singleton('Spoti', static function () {

            return new Main();
        });
    }
}
