<?php


$data = [];

$data['user_name'] = [
    'validation' => 'required',
    'label' => 'User name ID',
    'name' => 'user_name',
    'order' => 1,
    'type' => 'text' //todo add as constant
];

$data['key'] = [
    'validation' => 'required',
    'label' => 'key',
    'name' => 'key',
    'order' => 2,
    'type' => 'text' //todo add as constant
];

//$data['x_api_token'] = 'API_TOKEN="NWM4ODJhYWItZTY0ZS00MmZlLWJmNWUtYmY3Y2E0MjI0ZjY2OGI3OWI4NTQtOGQ5MS00YjkzLTkyZmEtZDYzOGZhYWQ2MDQxNWFhMWYzOTEtZGMyOC00YmE1LTg2YzUtODY1NGI1NzQ5NTEz"';

return $data;
