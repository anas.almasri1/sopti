<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is facade class for Helper packages
 */


namespace aqsat_integration_bnpl\spoti\Facade;


use Illuminate\Support\Facades\Facade;

/**
 * @method static \aqsat_integration_bnpl\spoti\api\Spoti spoti(string $debug = false, int $time_out = 30)
 */

class Spoti extends Facade{

    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: Get the registered name of the component.
     *
     ***************************************************************
     */
    protected static function getFacadeAccessor(){

        return 'Spoti';
    }
}
